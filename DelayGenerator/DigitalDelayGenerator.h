//=============================================================================
//
// file :        DigitalDelayGenerator.h
//
// description : Include for the DigitalDelayGenerator class.
//
// project :	Digital Delay Generator T560
//
// $Author: xavela $
//
// $Revision: 1.3 $
//
// $Log: not supported by cvs2svn $
// Revision 1.2  2010/03/25 18:13:11  vince_soleil
// "Migration_Tango7"
//
// Revision 1.1  2009/01/21 13:04:56  stephle
// initial import
//
//
// copyleft :    European Synchrotron Radiation Facility
//               BP 220, Grenoble 38043
//               FRANCE
//
//=============================================================================
//
//  		This file is generated by POGO
//	(Program Obviously used to Generate tango Object)
//
//         (c) - Software Engineering Group - ESRF
//=============================================================================
#ifndef _DIGITALDELAYGENERATOR_H
#define _DIGITALDELAYGENERATOR_H

#include <tango.h>
//using namespace Tango;
#include "DeviceProxyHelper.h"

/**
 * @author	$Author: xavela $
 * @version	$Revision: 1.3 $
 */

 //	Add your own constants definitions here.
 //-----------------------------------------------


namespace DigitalDelayGenerator_ns
{

/**
 * Class Description:
 * This device controls a T560 Digital Delay Generator :
 *	- four TTL-level pulse outputs, each programmable for delay and width up to 10 seconds each, with 10-picosecond resolution.
 *	- Programmable-level trigger input with divide/burst features and trigger enable GATE input.
 *	Controlled via a RS232 serial interface or a 10/100-mbps Ethernet interface.
 *	
 *	NOTE :
 *	- RS232 config : 38400, 8, N, 1
 *	- ETHERNET port : 2000
 */

/*
 *	Device States Description:
*  Tango::FAULT :  No communication with the T560 module.
*  Tango::ALARM :  The T560 module reports error(s).
*  Tango::ON :     The T560 module is OK.
 */


class DigitalDelayGenerator: public Tango::Device_4Impl
{
public :
	//	Add your own data members here
	//-----------------------------------------


	//	Here is the Start of the automatic code generation part
	//-------------------------------------------------------------	
/**
 *	@name attributes
 *	Attributs member data.
 */
//@{
		Tango::DevULong	*attr_shotsCounter_read;
		Tango::DevDouble	*attr_pulseDelayChannelA_read;
		Tango::DevDouble	attr_pulseDelayChannelA_write;
		Tango::DevDouble	*attr_pulseWidthChannelA_read;
		Tango::DevDouble	attr_pulseWidthChannelA_write;
		Tango::DevBoolean	*attr_activateChannelA_read;
		Tango::DevBoolean	attr_activateChannelA_write;
		Tango::DevDouble	*attr_pulseDelayChannelB_read;
		Tango::DevDouble	attr_pulseDelayChannelB_write;
		Tango::DevDouble	*attr_pulseWidthChannelB_read;
		Tango::DevDouble	attr_pulseWidthChannelB_write;
		Tango::DevBoolean	*attr_activateChannelB_read;
		Tango::DevBoolean	attr_activateChannelB_write;
		Tango::DevDouble	*attr_pulseDelayChannelC_read;
		Tango::DevDouble	attr_pulseDelayChannelC_write;
		Tango::DevDouble	*attr_pulseWidthChannelC_read;
		Tango::DevDouble	attr_pulseWidthChannelC_write;
		Tango::DevBoolean	*attr_activateChannelC_read;
		Tango::DevBoolean	attr_activateChannelC_write;
		Tango::DevDouble	*attr_pulseDelayChannelD_read;
		Tango::DevDouble	attr_pulseDelayChannelD_write;
		Tango::DevDouble	*attr_pulseWidthChannelD_read;
		Tango::DevDouble	attr_pulseWidthChannelD_write;
		Tango::DevBoolean	*attr_activateChannelD_read;
		Tango::DevBoolean	attr_activateChannelD_write;
//@}

/**
 *	@name Device properties
 *	Device properties member data.
 */
//@{
/**
 *	Name of the device which controls the communication link.
 *	This module support both RS232 and Ethernet interfaces.
 */
	string	communicationProxy;
//@}

/**@name Constructors
 * Miscellaneous constructors */
//@{
/**
 * Constructs a newly allocated Command object.
 *
 *	@param cl	Class.
 *	@param s 	Device Name
 */
	DigitalDelayGenerator(Tango::DeviceClass *cl,string &s);
/**
 * Constructs a newly allocated Command object.
 *
 *	@param cl	Class.
 *	@param s 	Device Name
 */
	DigitalDelayGenerator(Tango::DeviceClass *cl,const char *s);
/**
 * Constructs a newly allocated Command object.
 *
 *	@param cl	Class.
 *	@param s 	Device name
 *	@param d	Device description.
 */
	DigitalDelayGenerator(Tango::DeviceClass *cl,const char *s,const char *d);
//@}

/**@name Destructor
 * Only one desctructor is defined for this class */
//@{
/**
 * The object desctructor.
 */	
	~DigitalDelayGenerator() {delete_device();};
/**
 *	will be called at device destruction or at init command.
 */
	void delete_device();
//@}

	
/**@name Miscellaneous methods */
//@{
/**
 *	Initialize the device
 */
	virtual void init_device();
/**
 *	Always executed method befor execution command method.
 */
	virtual void always_executed_hook();

//@}

/**
 * @name DigitalDelayGenerator methods prototypes
 */

//@{
/**
 *	Hardware acquisition for attributes.
 */
	virtual void read_attr_hardware(vector<long> &attr_list);
/**
 *	Extract real attribute values for shotsCounter acquisition result.
 */
	virtual void read_shotsCounter(Tango::Attribute &attr);
/**
 *	Extract real attribute values for pulseDelayChannelA acquisition result.
 */
	virtual void read_pulseDelayChannelA(Tango::Attribute &attr);
/**
 *	Write pulseDelayChannelA attribute values to hardware.
 */
	virtual void write_pulseDelayChannelA(Tango::WAttribute &attr);
/**
 *	Extract real attribute values for pulseWidthChannelA acquisition result.
 */
	virtual void read_pulseWidthChannelA(Tango::Attribute &attr);
/**
 *	Write pulseWidthChannelA attribute values to hardware.
 */
	virtual void write_pulseWidthChannelA(Tango::WAttribute &attr);
/**
 *	Extract real attribute values for activateChannelA acquisition result.
 */
	virtual void read_activateChannelA(Tango::Attribute &attr);
/**
 *	Write activateChannelA attribute values to hardware.
 */
	virtual void write_activateChannelA(Tango::WAttribute &attr);
/**
 *	Extract real attribute values for pulseDelayChannelB acquisition result.
 */
	virtual void read_pulseDelayChannelB(Tango::Attribute &attr);
/**
 *	Write pulseDelayChannelB attribute values to hardware.
 */
	virtual void write_pulseDelayChannelB(Tango::WAttribute &attr);
/**
 *	Extract real attribute values for pulseWidthChannelB acquisition result.
 */
	virtual void read_pulseWidthChannelB(Tango::Attribute &attr);
/**
 *	Write pulseWidthChannelB attribute values to hardware.
 */
	virtual void write_pulseWidthChannelB(Tango::WAttribute &attr);
/**
 *	Extract real attribute values for activateChannelB acquisition result.
 */
	virtual void read_activateChannelB(Tango::Attribute &attr);
/**
 *	Write activateChannelB attribute values to hardware.
 */
	virtual void write_activateChannelB(Tango::WAttribute &attr);
/**
 *	Extract real attribute values for pulseDelayChannelC acquisition result.
 */
	virtual void read_pulseDelayChannelC(Tango::Attribute &attr);
/**
 *	Write pulseDelayChannelC attribute values to hardware.
 */
	virtual void write_pulseDelayChannelC(Tango::WAttribute &attr);
/**
 *	Extract real attribute values for pulseWidthChannelC acquisition result.
 */
	virtual void read_pulseWidthChannelC(Tango::Attribute &attr);
/**
 *	Write pulseWidthChannelC attribute values to hardware.
 */
	virtual void write_pulseWidthChannelC(Tango::WAttribute &attr);
/**
 *	Extract real attribute values for activateChannelC acquisition result.
 */
	virtual void read_activateChannelC(Tango::Attribute &attr);
/**
 *	Write activateChannelC attribute values to hardware.
 */
	virtual void write_activateChannelC(Tango::WAttribute &attr);
/**
 *	Extract real attribute values for pulseDelayChannelD acquisition result.
 */
	virtual void read_pulseDelayChannelD(Tango::Attribute &attr);
/**
 *	Write pulseDelayChannelD attribute values to hardware.
 */
	virtual void write_pulseDelayChannelD(Tango::WAttribute &attr);
/**
 *	Extract real attribute values for pulseWidthChannelD acquisition result.
 */
	virtual void read_pulseWidthChannelD(Tango::Attribute &attr);
/**
 *	Write pulseWidthChannelD attribute values to hardware.
 */
	virtual void write_pulseWidthChannelD(Tango::WAttribute &attr);
/**
 *	Extract real attribute values for activateChannelD acquisition result.
 */
	virtual void read_activateChannelD(Tango::Attribute &attr);
/**
 *	Write activateChannelD attribute values to hardware.
 */
	virtual void write_activateChannelD(Tango::WAttribute &attr);
/**
 *	Read/Write allowed for shotsCounter attribute.
 */
	virtual bool is_shotsCounter_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for pulseDelayChannelA attribute.
 */
	virtual bool is_pulseDelayChannelA_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for pulseWidthChannelA attribute.
 */
	virtual bool is_pulseWidthChannelA_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for activateChannelA attribute.
 */
	virtual bool is_activateChannelA_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for pulseDelayChannelB attribute.
 */
	virtual bool is_pulseDelayChannelB_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for pulseWidthChannelB attribute.
 */
	virtual bool is_pulseWidthChannelB_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for activateChannelB attribute.
 */
	virtual bool is_activateChannelB_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for pulseDelayChannelC attribute.
 */
	virtual bool is_pulseDelayChannelC_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for pulseWidthChannelC attribute.
 */
	virtual bool is_pulseWidthChannelC_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for activateChannelC attribute.
 */
	virtual bool is_activateChannelC_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for pulseDelayChannelD attribute.
 */
	virtual bool is_pulseDelayChannelD_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for pulseWidthChannelD attribute.
 */
	virtual bool is_pulseWidthChannelD_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for activateChannelD attribute.
 */
	virtual bool is_activateChannelD_allowed(Tango::AttReqType type);
/**
 *	Execution allowed for HardwareReset command.
 */
	virtual bool is_HardwareReset_allowed(const CORBA::Any &any);
/**
 *	Execution allowed for ClearErrors command.
 */
	virtual bool is_ClearErrors_allowed(const CORBA::Any &any);
/**
 *	Execution allowed for ResetShots command.
 */
	virtual bool is_ResetShots_allowed(const CORBA::Any &any);
/**
 *	Execution allowed for SaveModuleConfiguration command.
 */
	virtual bool is_SaveModuleConfiguration_allowed(const CORBA::Any &any);
/**
 *	Execution allowed for RecallModuleConfiguration command.
 */
	virtual bool is_RecallModuleConfiguration_allowed(const CORBA::Any &any);
/**
 * This command gets the device state (stored in its <i>device_state</i> data member) and returns it to the caller.
 *	@return	State Code
 *	@exception DevFailed
 */
	virtual Tango::DevState	dev_state();
/**
 * This command gets the device status (stored in its <i>device_status</i> data member) and returns it to the caller.
 *	@return	Status description
 *	@exception DevFailed
 */
	virtual Tango::ConstDevString	dev_status();
/**
 * Performs a hardware reset/restart of the T560 (equivalent to a power off/on cycle.
 *	NOTE : the reset takes about 4 seconds, after which the T560 will respond with the following string
 *	"Highland Thechnology T560 DDG <cr> <lf>"
 *	The last-saved setup will be installed.
 *	@exception DevFailed
 */
	void	hardware_reset();
/**
 * Clear error flag.
 *	@exception DevFailed
 */
	void	clear_errors();
/**
 * Reset shot counter.
 *	@exception DevFailed
 */
	void	reset_shots();
/**
 * Save the T560 module configuration in its intrenal flash memory.
 *	This configuartion is applied after a reset.
 *	@exception DevFailed
 */
	void	save_module_configuration();
/**
 * Reapply the previous module configuration. No need to do a reset.
 *	@exception DevFailed
 */
	void	recall_module_configuration();

/**
 *	Read the device properties from database
 */
	 void get_device_property();
//@}

	//	Here is the end of the automatic code generation part
	//-------------------------------------------------------------	



protected :	
	//	Add your own data members here
	//-----------------------------------------
	Tango::DeviceProxyHelper*	_communicationLink;
	bool _properties_missing;							//- check properties
	bool _init_done;									//- deffered device initialization
	bool _error;										//- check if T560 module is on error
	std::string _status;								//- device status

	//- Default module configuration :
	void apply_default_configuration(void);
	
	//- pulse delay methods :
	void read_pulseDelay(std::string channel, double& pDelay);
	void write_pulseDelay(std::string channel, double pDelay);
  //- pulse width methods :
  void read_pulseWidth(std::string channel, double& pulseWidth);
  void write_pulseWidth(std::string channel, double pulseWidth);
	//- channel activation
	bool isChannelActivated(std::string channel);
	void activateChannel(std::string channel, bool activation);

	std::string write_read(std::string cmd);
};

}	// namespace_ns

#endif	// _DIGITALDELAYGENERATOR_H
