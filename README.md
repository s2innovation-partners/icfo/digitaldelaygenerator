# Digital Delay Generator T560

## Prerequirements

The device is configured to obtain IP automatically, the port is set to 2000 by default. 
When having troubles connecting to the device please try to search for int inside the local network using https://www.lantronix.com/products/deviceinstaller/.

To launch the device server *CommunicationProxy* property must be specified. To obtain proper proxy appropiate Socket must be running.

## Running

To run the server please type the following command:

```./DigitalDelayGenerator [device name]```

The executable can be found in the main filesystem tree under /DelayGenerator inside docker container.
